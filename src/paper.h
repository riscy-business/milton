#pragma once
// State machine for paper
enum MiltonPaperFlags
{
    MiltonPaperFlags_PAPER_VISIBLE =  1 << 0,
    MiltonPaperFlags_LINE_MODE = 1 << 1
};
